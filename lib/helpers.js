module.exports = {
    click: async function(page, selector) {
        try {
            await page.waitForSelector(selector)
            await page.click(selector)
        } catch {
            throw new Error(`Could not click on selector: ${selector}`)
        }
    },
    getText: async function(page, selector) {
        try {
            // await page.setDefaultTimeout(4000)
            await page.waitForSelector(selector)
            return page.$eval(selector, element => element.textContent)
        } catch {
            throw new Error(`Could not get text from: ${selector}`)
        }
    },
    getCount: async function(page, selector) {
        try {
            await page.waitForSelector(selector)
            return page.$$eval(selector, element => element.length)
        } catch {
            throw new Error(`Could not count of element: ${selector}`)
        }
    },
    typeText: async function(page, selector, text) {
        try {
            await page.waitForSelector(selector)
            await page.type(selector, text)
        } catch (error) {
            throw new Error(`Could not type into selector : ${selector}`)
        }
    },
    waitForText: async function(page, selector, text) {
        try {
            await page.waitForSelector(selector)
            await page.waitForFunction((selector, text) => {
                document.querySelector(selector).innerText.includes(text),
                    {},
                    selector,
                    text
            })
        } catch (error) {
            throw new Error(`Text: ${text} not found for selector: ${selector}`)
        }
    },
    shouldNotExist: async function(page, selector) {
        try {
            await page.waitForSelector('#signin_button', {
                hidden:true,
                timeout: 100000
            })
        } catch (error) {

            throw new Error(`Selector ${selector} is visible but should not be`)
        }
    },
    selectOption: async function(page, selector, option) {
        try {
            await page.waitForSelector(selector)
            await page.select(selector, option)
        } catch {
            throw new Error(`Could not select option: ${option} of selector: ${selector}`)
        }
    },
    wait: async function(seconds) {
        return new Promise(function(resolve) {
            setTimeout(resolve, seconds * 1000)
        })
    },
    autoScroll: async function(page) {
        await page.evaluate(async () => {
            await new Promise((resolve) => {
                let totalHeight = 0;
                let distance = 100;
                let timer = setInterval(() => {
                    let scrollHeight = document.body.scrollHeight;
                    window.scrollBy(0, distance);
                    totalHeight += distance;

                    if (totalHeight >= scrollHeight) {
                        clearInterval(timer);
                        resolve();
                    }
                }, 100);
            });
        });
    }
}