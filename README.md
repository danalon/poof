# README #

Playwright testing environment for Sportland e-store

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Playwright testing environment setup notes ###

* clone repo
* run [ npm i ] from root folder
* run [ npm run testName ] from root folder - testNames are set in package.json; example - [ npm run test:orderPlacement ]
* run [ npm run testGroup ] from root folder - testGroups are set in package.json; example - [ npm run test:orderFlow ]

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
* View tests here: https://drive.google.com/drive/folders/1OVi3QJPWDFYEhcYQZUNXkfrWfZvcxLcd?usp=sharing

