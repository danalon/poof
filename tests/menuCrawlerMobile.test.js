/**
 * @name Click Through all Menu items in Mobile
 * @desc Breadcrumbs appear after being redirected to PLP and no two PLP's have same breadcrumb
 */
const playwright = require('playwright')

let browser
let page
const website = 'https://sportland.lt/'
let arrayOfPLPheaders = []
let fourOhFourLinks = []

/* List of Demo and Live websites
'https://demo-sportland.readymage.com/'
'https://sportland.com/'
'https://demo-sportland-ee.readymage.com/'
'https://sportland.ee/'
'https://demo-sportland-lt.readymage.com/'
'https://sportland.lt/'
'https://demo-sportland-lv.readymage.com/'
'https://sportland.lv/'
*/

before(async () => {
    browser = await playwright.chromium.launch({
        headless: false,
        slowMo: 100
    })
    const iPhone11 = playwright.devices["iPhone 11 Pro"]
    const context = await browser.newContext({
        ...iPhone11,
        geolocation: {
            longitude: 52.520008,
            latitude: 13.404954
        },
        permissions: ["geolocation"],
    })
    page = await context.newPage()
})

describe('Click Through all Menu items', function() {
    this.timeout(500000)

    it('Having an access to storefront', async () => {
        await page.goto(website)
        await page.waitForSelector('.NavigationTabs')
        console.log('----------------------------------------------------------')
        console.log('Website : ' + website)
    })

    it('Accept Cookies button appears', async () => {
        await page.waitForSelector('#root > div.CookiePopup > section > div > div > button')
    })

    it('Clicking on Accept Cookies button', async () => {
        await page.click('#root > div.CookiePopup > section > div > div > button')
    })

    it('Hamburger button for Menu (mobile) appears', async () => {
        await page.waitForSelector(
            '.NavigationTabs > .NavigationTabs-Nav > .NavigationTabs-Button > .NavigationTabs-Burger > .line-2'
        )
    })

    it('Clicking on hamburger button for Menu (mobile) ', async () => {
        await page.click(
            '.NavigationTabs > .NavigationTabs-Nav > .NavigationTabs-Button > .NavigationTabs-Burger > .line-2'
        )
    })

    it('iframe for chat appears', async () => {
        await page.waitForSelector("div#fc_frame")
    })

    it('Getting access and closing chat iframe', async () => {
        const elementHandle = await page.$('div#fc_frame iframe')
        const frame = await elementHandle.contentFrame()
        await frame.waitForSelector('.d_preview')
        await frame.click('.d_preview')
        await page.waitForTimeout(250)
        await frame.waitForSelector('.icon.icon-ic_close.mild')
        await frame.click('.icon.icon-ic_close.mild')
    })

    it('Clicking through all Menu items', async () => {

        let onesLength = (await page.$$('#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li')).length

        for (let one = 1; one < onesLength + 1; one++) {

            // TODO Menu item brands is skipped - could not make test to see menu items under Brands
            if (one == 4) continue

            let oneSelector
            if (one == 5) {
                oneSelector = `#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(5) > a > figure > figcaption`
            } else {
                oneSelector = `#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(${one}) > div > figure > figcaption`
            }

            await page.waitForSelector(oneSelector)
            let selector = await page.$(oneSelector)

            let innerText = await page.evaluate(el => el.textContent, selector)
            console.log('----------------------------------------------------------')
            console.log('├── ' + innerText)

            await page.waitForSelector(`#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(${one})`)
            await page.click(`#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(${one})`)

            if (one == 5) {
                await page.waitForSelector('#root > main > section > div > h1')
                await page.waitForTimeout(650)
                let selector = await page.$('#root > main > section > div > h1')
                let innerText = await page.evaluate(el => el.textContent, selector)

                if (innerText.includes('404')) await fourOhFourLinkGatherer(innerText, page.url())
                console.log('├── ├── ├── ' + innerText)
                arrayOfPLPheaders.push(innerText)
            }

            let twosLength = (
                await page.$$(
                    `#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(${one}) > div > div > div > div`
                )
            ).length

            for (let two = 1; two < twosLength + 1; two++) {
                if (one == 5) break

                await page.waitForSelector(
                    `#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(${one}) > div > div > div > div:nth-child(${two}) > figure > figcaption`
                )
                let selector = await page.$(
                    `#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(${one}) > div > div > div > div:nth-child(${two}) > figure > figcaption`
                )
                let innerText = await page.evaluate(el => el.textContent, selector)

                console.log('----------------------------------------------------------')
                console.log('├── ├──  ' + innerText)
                console.log('----------------------------------------------------------')

                await page.click(
                    `#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(${one}) > div > div > div > div:nth-child(${two}) > figure > figcaption`
                )
                let threesLength = (
                    await page.$$(
                        `#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(${one}) > div > div > div > div:nth-child(${two}) > div > div > a`
                    )
                ).length

                for (let three = 1; three < threesLength + 1; three++) {
                    await page.waitForSelector(
                        `#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(${one}) > div > div > div > div:nth-child(${two}) > div > div > a:nth-child(${three}) > figure > figcaption`
                    )
                    await page.click(
                        `#root > main > div > div.MenuOverlay-Menu.isMobile > ul > li:nth-child(${one}) > div > div > div > div:nth-child(${two}) > div > div > a:nth-child(${three}) > figure > figcaption`
                    )

                    await page.waitForSelector('#root > main > section > div > h1')
                    await page.waitForTimeout(650)
                    let selector = await page.$('#root > main > section > div > h1')
                    let innerText = await page.evaluate(el => el.textContent, selector)

                    if (innerText.includes('404')) await fourOhFourLinkGatherer(innerText, page.url())
                    console.log('├── ├── ├── ' + innerText)
                    arrayOfPLPheaders.push(innerText)

                    await page.waitForSelector('body > #root > .Header > .Header-Nav > .Header-Button_isVisible')
                    await page.click('body > #root > .Header > .Header-Nav > .Header-Button_isVisible')
                }

                await page.waitForSelector('body > #root > .Header > .Header-Nav > .Header-Button_isVisible')
                await page.click('body > #root > .Header > .Header-Nav > .Header-Button_isVisible')
            }

            await page.waitForSelector('body > #root > .Header > .Header-Nav > .Header-Button_isVisible')
            await page.click('body > #root > .Header > .Header-Nav > .Header-Button_isVisible')
        }

        const doesArrayHaveDuplicates = arrayOfPLPheaders.some(
            (val, i) => arrayOfPLPheaders.indexOf(val) !== i
        )

        console.log('----------------------------------------------------------')
        console.log('Total amount of categories in Menu: ' + arrayOfPLPheaders.length)
        console.log('Did automation script run into two identical breadcrumbs on PLP: ' + doesArrayHaveDuplicates)
        console.log('----------------------------------------------------------')
        console.log('How many 404 in opened PLP pages: ' + fourOhFourLinks.length)
        if (fourOhFourLinks.length > 0) {
            for (let fourOhFourLink of fourOhFourLinks) {
                console.log(fourOhFourLink)
            }
        }
        console.log('-----------------------------------------------------------------------------------------------------------------------------------------')
        console.log(' !!!!! Menu items under Brands should be checked manually - for now automated test can\'t access those particular element handlers !!!!! ')
        console.log('-----------------------------------------------------------------------------------------------------------------------------------------')

        async function fourOhFourLinkGatherer(header, url) {
            let trimmedHeader = header.trim()
            if (trimmedHeader == '404') fourOhFourLinks.push(url)
        }
    })
})

after(async () => {
    await browser.close()
})