/**
 * @name Click Through all Footer links
 * @desc Lists of 404 and Empty Product Lists URLs are returned
 */
const playwright = require('playwright')

let browser
let page
const website =
    ['https://sportland.lv/', '/kategorija']

let arrayOfPLPheaders = []
let fourOhFourLinks = []
let emptyPLPLinks = []

/* List of Demo and Live websites
    ['https://sportland.com/', '/category']
    ['https://sportland.ee/', '/kategooria']
    ['https://sportland.lt/', '/kategorija']
    ['https://sportland.lv/', '/kategorija']

    ['https://demo-sportland.readymage.com/', '/category']
    ['https://demo-sportland-ee.readymage.com/', '/kategooria']
    ['https://demo-sportland-lt.readymage.com/', '/kategorija']
    ['https://demo-sportland-lv.readymage.com/', '/kategorija']
*/

/* List of Demo and Live websites
'https://demo-sportland.readymage.com/'
'https://sportland.com/'
'https://demo-sportland-ee.readymage.com/'
'https://sportland.ee/'
'https://demo-sportland-lt.readymage.com/'
'https://sportland.lt/'
'https://demo-sportland-lv.readymage.com/'
'https://sportland.lv/'
*/

before(async () => {
    browser = await playwright.chromium.launch({
        headless: false
    })
    page = await browser.newPage()

    await page.setViewportSize({
        width: 1920 * 0.75,
        height: 1080 * 0.75
    })
})

describe('Click Through all Menu items', function () {
    this.timeout(500000)

    it('Having an access to storefront', async () => {
        await page.goto(website[0])
        await page.waitForSelector('.Header-Button.Header-Button_type_account')
    })

    it('Having an access to Menu', async () => {
        await page.waitForSelector('.MenuOverlay-Menu')
    })

    it('Clicking through all Footer items', async () => {
        const onesLength = (await page.$$('#root > footer > div > div > div > div > div > .footer > nav > div')).length
        console.log(onesLength)
        console.log('---------------------------------------------------------------------')
        console.log(website[0])

        for (let j = 1; j < onesLength + 1; j++) {
            let selector = await page.$(
                `#root > footer > div > div > div > div > div > .footer > nav > div:nth-child(${j}) > h3`
            )
            let innerText = await page.evaluate(el => el.textContent, selector)
            console.log('---------------------------------------------------------------------')
            console.log('├── ' + innerText + await doesURLHaveCategoryInIt(page.url()))

            let twosLength = 0
            if (j == 1) {
                twosLength = (await page.$$(`#root > footer > div > div > div > div > div > .footer > nav > div.footer-nav_section.Footer-Contacts > p:nth-child(4) > small > a`)).length
            } else {
                twosLength = (await page.$$(`#root > footer > div > div > div > div > div > .footer > nav > div:nth-child(${j}) > ul > li > a`)).length
            }

            for (let i = 1; i < twosLength + 1; i++) {
                let css = ''
                if (j == 1) {
                    css = `#root > footer > div > div > div > div > div > .footer > nav > div.footer-nav_section.Footer-Contacts > p:nth-child(4) > small > a:nth-child(${i})`
                } else {
                    css = `#root > footer > div > div > div > div > div > .footer > nav > div:nth-child(${j}) > ul > li:nth-child(${i}) > a`
                }

                let selector = await page.$(
                    css
                )
                let innerText = await page.evaluate(el => el.textContent, selector)
                await page.waitForSelector(css)
                await page.$eval(css, e => e.setAttribute("target", "_top"))
                await page.click(css)
                await page.waitForTimeout(650)

                let breadcrumbOrURL = ''
                if (await page.$('#root > main > section > div > h1') !== null) {
                    await page.waitForTimeout(650)
                    let breadcrumbSelector = await page.$('#root > main > section > div > h1')
                    breadcrumbOrURL = await page.evaluate(el => el.textContent, breadcrumbSelector)
                } else {
                    breadcrumbOrURL = page.url()
                }


                if (breadcrumbOrURL.includes('404')) await fourOhFourLinkGatherer(breadcrumbOrURL, page.url())

                let aaa = ((await page.$$('.CategoryProductList-ProductsMissing')).length)
                if(aaa > 0){
                    console.log((await page.$$('.CategoryProductList-ProductsMissing')).length + '  ---  ' + page.url())
                    await emptyPLPLinkGatherer(page.url())
                }

                arrayOfPLPheaders.push(breadcrumbOrURL)

                console.log('├── ├── ' + innerText + ' -> ' + breadcrumbOrURL + await doesURLHaveCategoryInIt(page.url()))
                await page.goto(website[0])
            }
        }
        console.log('---------------------------------------------------------------------')

        const doesArrayHaveDuplicates = arrayOfPLPheaders.some(
            (val, i) => arrayOfPLPheaders.indexOf(val) !== i
        )

        console.log('---------------------------------------------------------------------')
        console.log('Did automation script run into two or more identical breadcrumbs in PLP\'s: ' + doesArrayHaveDuplicates)
        console.log('---------------------------------------------------------------------')
        console.log('How many 404 in opened PLP pages: ' + fourOhFourLinks.length)
        console.log('---------------------------------------------------------------------')
        if (fourOhFourLinks.length > 0) {
            for (let fourOhFourLink of fourOhFourLinks) {
                console.log(fourOhFourLink)
            }
        }

        console.log('----------------------------------------------------------')
        console.log('How many EMPTY product list pages test ran in to: ' + emptyPLPLinks.length)

        if (emptyPLPLinks.length > 0) {
            for (let emptyPLPLink of emptyPLPLinks) {
                console.log(emptyPLPLink)
            }
        }

        async function fourOhFourLinkGatherer(header, url) {
            let trimmedHeader = header.trim()
            if (trimmedHeader == '404') fourOhFourLinks.push(url)
        }

        async function doesURLHaveCategoryInIt(url) {
            if(url.includes(website[1])) {
                return (` / [URL with name "${website[1]}" in it : ` +  url + "]")
            } else {
                return ''
            }
        }
    })
})

after(async () => {
    await browser.close()
})