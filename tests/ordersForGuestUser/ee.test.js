/**
 * @name Order placements for Guest user with all Shipping methods for demo.ee
 * @desc Order number appearing after: successful login -> product addition to cart -> checkout -> payment completion
 */
const playwright = require('playwright')

let browser
let page
const website =
    ['https://demo-sportland-ee.readymage.com/']

/*
    ['https://demo-sportland-ee.readymage.com/']
    ['https://sportland.ee/', 'exit-before-placing-order']
*/

const phone = '+3722333333'
const headless = false
const shippingMethods = [
    'Carrier',
    'Parcel terminal'
]
const shippingOptions = [
    'Omniva Courier',
    'DPD Courier',
    'Omniva Locker',
    'DPD Locker'
]
const paymentMethod = 1
/* Payment methods and according indexes
    1 = Check / Money order
    2 = Cash On Delivery
    3 = Mastercard / Visa
    4 = Bank or Credit Card
 */

let counter = -1

before(async () => {
    browser = await playwright.chromium.launch({
        headless: headless
    })
    page = await browser.newPage()

    await page.setViewportSize({
        width: 1920,
        height: 1080 * 2
    })
})

for(let a = 1; a <= 2; a++){
    for(let b = 1; b <= 2; b++){
        counter++
        if(counter == 0 && website[1]) continue
        describe(`Order placement for Guest user with: ${shippingOptions[counter]}, for ${website[0]}`, function() {
            this.timeout(10000)

            it('Having an access to storefront', async () => {
                await page.goto(website[0])

                await page.waitForSelector('.MenuOverlay-Item')
            }).timeout(15000)

            it('Clicking on first item listed in Main Menu', async () => {
                await page.click(
                    '#root > header > nav > div.MenuOverlay-Container > div > div > ul > li')
            })

            it('PLP page appears', async () => {
                await page.waitForSelector(
                    '.ProductCard')
            })

            it('Clicking on first item listed in PLP', async () => {
                await page.click('.ProductCard > .ProductCard-Link')
            })

            it('PDP page appears', async () => {
                await page.waitForSelector('.ProductActions-AddToCartWrapper')
            })

            it('Clicking through all available size options', async () => {
                await page.waitForSelector('a[aria-hidden="false"].ProductAttributeValue')
                const sizes = await page.$$('a[aria-hidden="false"].ProductAttributeValue')
                for(const size of sizes){
                    await size.click()
                    break
                }
            })

            it('Clicking on ADD TO CART button', async () => {
                await page.click(
                    '.ProductActions-Wrapper > .ProductActions > .ProductActions-AddToCartWrapper > .Button > ' +
                    'span:nth-child(1)')
            })

            it('Pop-up with chosen product info and CHECKOUT button appears', async () => {
                await page.waitForSelector('.Popup-Content')
            })

            it('Clicking on CHECKOUT button', async () => {
                await page.click(
                    '.Popup-Content > .AddToCartPopup-Wrapper > .AddToCartPopup-Content > .AddToCartPopup-ButtonBlock > ' +
                    '.AddToCartPopup-CheckoutButton')
            })

            it('Sign in / Registration page appears', async () => {
                await page.waitForSelector('.PreCheckout-Footer')
            })

            it('Clicking on GUEST button', async () => {
                await page.click(
                    '.PreCheckout-Footer > .PreCheckout-RegistrationOptions > .PreCheckout-RegistrationOptions-Buttons > ' +
                    '.RegistrationOption > .Button'
                )
            })

            it('Select shipping method / address page appears', async () => {
                await page.waitForSelector('.Checkout-Heading')
            })

            it('Clicking on CHOOSE DELIVERY dropdown button', async () => {
                await page.click('div > div > .CheckoutDeliveryOptions-Select > .CheckoutDeliveryOptions-TypeListOption > span')
            })

            it(`Clicking on ${shippingMethods[a-1]}`, async () => {
                await page.click(
                    `div > div > .CheckoutDeliveryOptions-TypeList > .CheckoutDeliveryOptions-TypeListOption:nth-child(${a}) >  button`)
            })

            it(`Clicking on ${shippingOptions[counter]}`, async () => {
                let c = b
                if(website[1] && a == 1) c -= 1
                await page.click(
                    `.CheckoutDeliveryOptions-OptionsWrapper > .CheckoutDeliveryOption:nth-child(${c}) > .CheckoutDeliveryOption-Button > .CheckoutDeliveryOption-Row > span`)
            })

            if(a == 2){
                it('Clicking on first Locker option ', async () => {
                    await page.waitForSelector('div > .CheckoutDeliveryOption-Row2 > .CheckoutDeliveryOption-Autosuggestions > .CheckoutDeliveryOption-InputContainer > span')
                    await page.click('div > .CheckoutDeliveryOption-Row2 > .CheckoutDeliveryOption-Autosuggestions > .CheckoutDeliveryOption-InputContainer > span')

                    await page.waitForSelector('.CheckoutDeliveryOption-Autosuggestions > .CheckoutDeliveryOption-LocationsContainer > .CheckoutDeliveryOption-SearchList > ul > li:nth-child(1)')
                    await page.click('.CheckoutDeliveryOption-Autosuggestions > .CheckoutDeliveryOption-LocationsContainer > .CheckoutDeliveryOption-SearchList > ul > li:nth-child(1)')
                })
            }

            it('Filling EMAIL field with valid info', async () => {
                if(!headless) await page.waitForTimeout(1000)
                await page.fill('#guest_email', 'test@automation.com')
            })

            it('Filling FIRST NAME field with valid info', async () => {
                await page.fill('#firstname', 'Test')
            })

            it('Filling LAST NAME field with valid info', async () => {
                await page.fill('#lastname', 'Automation')
            })

            it('Filling PHONE field with valid info', async () => {
                await page.fill('#telephone', phone)
            })


            if(a != 2) {
                it('Filling STREET ADDRESS field with valid info', async () => {
                    await page.fill('#street', 'My Street')
                })

                it('Filling CITY field with valid info', async () => {
                    await page.fill('#city', 'City')
                })

                it('Filling ZIP/POSTAL CODE field with valid info', async () => {
                    await page.fill('#postcode', '13001')
                })
            }

            it('Clicking on PROCEED TO BILLING button', async () => {
                await page.click('#SHIPPING_STEP > div.Checkout-StickyButtonWrapper > button')
            })

            it('Billing Step page appears with My billing and shipping are the same checkbox and Check / Money order ' +
                'payment method radio-button pre- selected', async () => {
                await page.waitForSelector('#root > main > section > div > div.Checkout-Step > h1')
            })

            if(!website[1]) {
                it('Choosing payment method', async () => {
                    await page.waitForSelector(`#BILLING_STEP > .CheckoutPayments > .CheckoutPayments-Methods > .CheckoutPayment:nth-child(${paymentMethod}) > .CheckoutPayment-Button`)
                    await page.click(`#BILLING_STEP > .CheckoutPayments > .CheckoutPayments-Methods > .CheckoutPayment:nth-child(${paymentMethod}) > .CheckoutPayment-Button`)
                })

                if(paymentMethod == 4){
                    it('Choose 1st Bank to pay with', async () => {
                        await page.waitForSelector('.MakecommercePayment > .MakecommercePayment-Channels > .MakecommercePayment-PaymentChannel:nth-child(1) > .Image > .Image-Image')
                        await page.click('.MakecommercePayment > .MakecommercePayment-Channels > .MakecommercePayment-PaymentChannel:nth-child(1) > .Image > .Image-Image')
                    })
                }

                it('Checking I agree to terms and conditions checkbox', async () => {
                    await page.waitForSelector('#BILLING_STEP > div.CheckoutBilling-TermsAndConditions > div > label')
                    await page.click('#BILLING_STEP > div.CheckoutBilling-TermsAndConditions > div > label')
                })

                it('Clicking on COMPLETE ORDER button', async () => {
                    await page.click('#BILLING_STEP > div.Checkout-StickyButtonWrapper > button')
                })

                if(paymentMethod == 3){
                    it('Filling in Credit Card info', async () => {
                        await page.waitForSelector('#hps-pan')
                        await page.fill('#hps-pan', '4111111111111111')

                        await page.click('select#exp_month')
                        await page.keyboard.press('ArrowDown');

                        await page.click('#exp_year_dummy')
                        await page.keyboard.press('ArrowDown')
                        await page.keyboard.press('ArrowDown')

                        await page.waitForSelector('#hps-cvv')
                        await page.type('#hps-cvv', '111')

                        await page.waitForSelector('#hps-continue')
                        await page.click('#hps-continue')

                        await page.waitForSelector('input[value="Authenticated"]')
                        await page.click('input[value="Authenticated"]')
                    })
                }

                if(paymentMethod == 4){
                    it('Completing payment', async () => {
                        await page.waitForSelector('#wrap > .container > fieldset > form > .btn-success')
                        await page.click('#wrap > .container > fieldset > form > .btn-success')

                        await page.waitForTimeout(1000)

                        await page.waitForSelector('body > #wrap > .container > #submit-form > .btn')
                        await page.click('body > #wrap > .container > #submit-form > .btn')
                    })
                }

                it('Thank you for your purchase! page appears', async () => {
                    // await page.waitForTimeout(5000)
                    await page.waitForSelector('#root > main > section > div > div > div > h3')
                    const orderNumber = await page.$eval(
                        '#root > main > section > div > div > div > h3',
                        el => el.innerText.match(/(\d+)/)
                    )
                    console.log('Order number: ' + orderNumber[0])
                })
            }
        })
    }
}

after(async () => {
    await browser.close()
})