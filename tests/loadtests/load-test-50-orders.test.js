/**
 * @name Load test website
 * @desc Info about test length and success rate per instance appears
 */
const {
    Cluster
} = require("puppeteer-cluster");
(async () => {

    const website =
        'https://test-sportland-lv.readymage.com/'

    /*
        'https://test-sportland-lv.readymage.com/'
        'https://demo-sportland.readymage.com'
    */

    //Create cluster with 10 workers
    const cluster = await Cluster.launch({
        concurrency: Cluster.CONCURRENCY_CONTEXT,
        maxConcurrency: 50,
        monitor: true,
        timeout: 500000
    });

    // Print errors to console
    cluster.on("taskerror", (err, data) => {
        console.log(`Error crawling ${data}: ${err.message}`);
    });

    // Dumb sleep function to wait for page load
    async function timeout(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    await cluster.task(async ({
                                  page,
                                  data: url,
                                  worker
                              }) => {
        await page.goto(url, {
            waitUntil: 'load',
            timeout: 600000
        })
        await page.setViewport({
            width: 1920,
            height: 1080 * 2
        })

        await page.setDefaultNavigationTimeout(600000)

        await page.waitForSelector('.MenuOverlay-Item')
        await page.click(
            '#root > header > nav > div.MenuOverlay-Container > div > div > ul > li')


        await page.waitForSelector(
            '.ProductCard > .ProductCard-Link')
        await page.click('.ProductCard > .ProductCard-Link')

        await page.waitForSelector('a[aria-hidden="false"].ProductAttributeValue')
        const sizes = await page.$$('a[aria-hidden="false"].ProductAttributeValue')

        for (const size of sizes) {
            await size.click()
            break
        }

        await page.waitForSelector('.ProductActions-AddToCartWrapper')
        await page.click(
            '.ProductActions-Wrapper > .ProductActions > .ProductActions-AddToCartWrapper > .Button > ' +
            'span:nth-child(1)')
        await page.waitForSelector('.Popup-Content')
        await page.click(
            '.Popup-Content > .AddToCartPopup-Wrapper > .AddToCartPopup-Content > .AddToCartPopup-ButtonBlock > ' +
            '.AddToCartPopup-CheckoutButton')
        await page.waitForSelector('.PreCheckout-Footer')


        await page.waitForSelector('.PreCheckout-Footer > .PreCheckout-RegistrationOptions > .PreCheckout-RegistrationOptions-Buttons > ' +
            '.RegistrationOption > .Button'
        )

        await page.click(
            '.PreCheckout-Footer > .PreCheckout-RegistrationOptions > .PreCheckout-RegistrationOptions-Buttons > ' +
            '.RegistrationOption > .Button'
        )

        await page.waitForSelector('div > div > .CheckoutDeliveryOptions-Select > .CheckoutDeliveryOptions-TypeListOption > span')
        await page.click('div > div > .CheckoutDeliveryOptions-Select > .CheckoutDeliveryOptions-TypeListOption > span')

        await page.click(
            `div > div > .CheckoutDeliveryOptions-TypeList > .CheckoutDeliveryOptions-TypeListOption:nth-child(1) >  button`)

        await page.click(
            `.CheckoutDeliveryOptions-OptionsWrapper > .CheckoutDeliveryOption:nth-child(1) > .CheckoutDeliveryOption-Button > .CheckoutDeliveryOption-Row > span`)

        await page.waitForSelector('#guest_email')
        await page.type('#guest_email', 'test@automation.com')

        await page.type('#firstname', 'Test')

        await page.type('#lastname', 'Automation')

        await page.type('#telephone', '+37125555555')

        await page.type('#street', 'My Street')

        await page.type('#city', 'City')

        await page.type('#postcode', 'LV-3001')

        await page.click('#SHIPPING_STEP > div.Checkout-StickyButtonWrapper > button')

        await page.waitForSelector('#root > main > section > div > div.Checkout-Step > h1')

        await page.waitForSelector(`#BILLING_STEP > .CheckoutPayments > .CheckoutPayments-Methods > .CheckoutPayment:nth-child(1) > .CheckoutPayment-Button`)
        await page.click(`#BILLING_STEP > .CheckoutPayments > .CheckoutPayments-Methods > .CheckoutPayment:nth-child(1) > .CheckoutPayment-Button`)

        await page.waitForSelector('#BILLING_STEP > div.CheckoutBilling-TermsAndConditions > div > label')
        await page.click('#BILLING_STEP > div.CheckoutBilling-TermsAndConditions > div > label')

        await page.waitForTimeout(1000)

        // const currentDate = new Date()
        // const timestamp = currentDate.getTime()
        await page.screenshot({
            path: `screenshot.png`
        })
    });

    for (let i = 1; i <= 10; i++) {
        cluster.queue(website);
    }
    await cluster.idle();
    await cluster.close();
})();