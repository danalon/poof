/**
 * @name Load test website
 * @desc Info about test length and success rate per instance appears
 */
const {
    Cluster
} = require("puppeteer-cluster");
(async () => {

    const website =
        'https://test-sportland-lv.readymage.com/'

    /*
        'https://test-sportland-lv.readymage.com/'
        'https://demo-sportland.readymage.com'
    */

    //Create cluster with 10 workers
    const cluster = await Cluster.launch({
        concurrency: Cluster.CONCURRENCY_CONTEXT,
        maxConcurrency: 50,
        puppeteerOptions: {
            headless: true
        },
        monitor: true,
        timeout: 500000
    });

    // Print errors to console
    cluster.on("taskerror", (err, data) => {
        console.log(`Error crawling ${data}: ${err.message}`);
    });

    // Dumb sleep function to wait for page load
    async function timeout(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    await cluster.task(async ({
                                  page,
                                  data: url,
                                  worker
                              }) => {
        await page.goto(url, {
            waitUntil: 'load',
            timeout: 600000
        })
        await page.setViewport({
            width: 1920,
            height: 1080 * 2
        })

        let arrayOfPLPHeaders = []
        let fourOhFourLinks = []
        let emptyPLPLinks = []

        await page.setDefaultNavigationTimeout(600000)

        await page.waitForSelector('.Header-Button.Header-Button_type_account')

        await page.waitForSelector('.MenuOverlay-Menu')

        let counter = 0
        const ones = await page.$$('.MenuOverlay-ItemList.MenuOverlay-ItemList_type_main > li')
        await page.waitForTimeout(1000)
        console.log('--------------------------------------------------------------------------------------------------------------------')
        console.log('Website : ' + website[0])

        for (const one of ones) {
            counter++
            // if(counter == 1) continue
            // if(counter == 2) continue
            // if(counter == 3) continue
            // if(counter == 4) continue
            await one.hover()
            await one.click()
            await page.waitForSelector('div > h1')
            await page.waitForTimeout(1250)
            let element = await page.$('div > h1')
            let menuName = (await page.$('div > h1') !== null ? await page.evaluate(el => el.textContent, element) : 'BREADCRUMB HEADER CAN\'T BE READ')
            if(!menuName.includes('404')) arrayOfPLPHeaders.push(menuName)
            console.log('----------------------------------------------------------')
            console.log('├── ' + menuName + await doesURLHaveCategoryInIt(page.url()))
            menuName = 'could not read the PLP breadcrumb'
            if (counter > 4) continue

            const twos = await one.$$('div > div > div > div[role="button"]')
            await page.hover('#root > header > nav > div.MenuOverlay-Container > div > div > ul > li:nth-child(2) > div > a')
            await page.hover('#root > header > nav > div.MenuOverlay-Container > div > div > ul > li:nth-child(1) > div > a')
            await one.hover()

            for (const two of twos) {
                await page.waitForTimeout(3000)
                const threes = await two.$$('div > div > a')

                if (counter != 4) {
                    const clickable = await two.$$('a')
                    await page.hover('a')
                    await page.hover(`#root > header > nav > div.MenuOverlay-Container > div > div > ul > li:nth-child(${counter + 1}) > div > a`)
                    await page.hover(`#root > header > nav > div.MenuOverlay-Container > div > div > ul > li:nth-child(${counter}) > div > a`)
                    await one.hover()
                    await clickable[0].click()
                    await page.waitForSelector('div > h1')
                    await page.waitForTimeout(1000)
                    let subElement = await page.$('div > h1')
                    let subMenuName = (await page.$('div > h1') !== null ? await page.evaluate(el => el.textContent, subElement) : 'BREADCRUMB HEADER CAN\'T BE READ')
                    if(!subMenuName.includes('404')) arrayOfPLPHeaders.push(subMenuName)
                    console.log('----------------------------------------------------------')
                    console.log('├── ├──  ' + subMenuName + await doesURLHaveCategoryInIt(page.url()))
                    subMenuName = 'could not read the PLP breadcrumb'
                    console.log('----------------------------------------------------------')
                    await page.hover('a')
                    await one.hover()
                }

                for (const three of threes) {
                    await page.hover('a')
                    await one.hover()
                    await three.click()
                    await page.waitForSelector('div > h1')
                    await page.waitForTimeout(1000)
                    let subSubElement = await page.$('div > h1')
                    let subSubMenuName = (await page.$('div > h1') !== null ? await page.evaluate(el => el.textContent, subSubElement) : 'BREADCRUMB HEADER CAN\'T BE READ!')

                    let aaa = ((await page.$$('.CategoryProductList-ProductsMissing')).length)
                    if(aaa > 0){
                        console.log((await page.$$('.CategoryProductList-ProductsMissing')).length + '  ---  ' + page.url())
                        await emptyPLPLinkGatherer(page.url())
                    }


                    if (subSubMenuName.includes('404')) await fourOhFourLinkGatherer(subSubMenuName, page.url())

                    console.log('├── ├── ├── ' + subSubMenuName + await doesURLHaveCategoryInIt(page.url()))
                    if(!subSubMenuName.includes('404')) arrayOfPLPHeaders.push(subSubMenuName)

                    if(!subSubMenuName.includes('|') && subSubMenuName !== 'BREADCRUMB HEADER CAN\'T BE READ!') {
                        await page.waitForSelector('div > h1')
                        await page.waitForTimeout(1000)
                        const urls = await page.$$eval("figure > a", el => el.map(x => x.getAttribute("href")))
                        for(const url of urls){
                            const page2 = await browser.newPage()
                            await page2.setDefaultTimeout(600000)
                            await page2.setViewportSize({
                                width: 1920,
                                height: 2080
                            })
                            await page2.goto((url.search(website[0]) !== -1 ? '' : website[0]) + url, {waitUntil: 'load', timeout: 0})
                            await page2.waitForSelector('div > h1')
                            await page2.waitForTimeout(1000)
                            let subElement = await page2.$('div > h1')
                            let subMenuName = (await page2.$('div > h1') !== null ? await page2.evaluate(el => el.textContent, subElement) : 'BREADCRUMB HEADER CAN\'T BE READ')
                            for(let i = 0; i < 10; i++) {
                                if(subMenuName.length < 1) {
                                    await page.waitForTimeout(500)
                                    subMenuName = (await page2.$('div > h1') !== null ? await page2.evaluate(el => el.textContent, subElement) : 'BREADCRUMB HEADER CAN\'T BE READ')
                                }
                            }

                            let bbb = ((await page2.$$('.CategoryProductList-ProductsMissing')).length)
                            if(bbb > 0){
                                console.log((await page2.$$('.CategoryProductList-ProductsMissing')).length + '  ---  ' + page2.url())
                                await emptyPLPLinkGatherer(page2.url())
                            }

                            if(!subMenuName.includes('404')) arrayOfPLPHeaders.push(subMenuName)
                            if (subMenuName.includes('404')) {
                                await fourOhFourLinkGatherer(subMenuName, page2.url())
                                await page.waitForTimeout(3000)
                            }
                            console.log('├── ├── ├── ├── ' + subMenuName + await doesURLHaveCategoryInIt(page.url()))
                            subMenuName = 'could not read the PLP breadcrumb'
                            await page2.close()
                        }
                        console.log('===============================================================================================')
                    }
                }
            }
        }

        const doesArrayHaveDuplicates = arrayOfPLPHeaders.some(
            (val, i) => arrayOfPLPHeaders.indexOf(val) !== i
        )
        console.log('----------------------------------------------------------')
        console.log('Total amount of categories in Menu: ' + arrayOfPLPHeaders.length)
        console.log('Did automation script run into two identical breadcrumbs on PLP: ' + doesArrayHaveDuplicates)
        console.log('----------------------------------------------------------')
        console.log('How many 404 in opened PLP pages: ' + fourOhFourLinks.length)

        if (fourOhFourLinks.length > 0) {
            for (let fourOhFourLink of fourOhFourLinks) {
                console.log(fourOhFourLink)
            }
        }

        console.log('----------------------------------------------------------')
        console.log('How many EMPTY product list pages test ran in to: ' + emptyPLPLinks.length)

        if (emptyPLPLinks.length > 0) {
            for (let emptyPLPLink of emptyPLPLinks) {
                console.log(emptyPLPLink)
            }
        }

        async function fourOhFourLinkGatherer(header, url) {
            let trimmedHeader = header.trim()
            for(const fourOhFourLink of fourOhFourLinks) {
                if(fourOhFourLink == trimmedHeader) {
                    trimmedHeader = ''
                    break
                }
            }
            if (trimmedHeader == '404') fourOhFourLinks.push(url)
        }

        async function emptyPLPLinkGatherer(url) {
            emptyPLPLinks.push(url)
        }

        async function doesURLHaveCategoryInIt(url) {
            // if(url.includes(website[1])) {
            //     return (` / [URL with name "${website[1]}" in it : ` +  url + "]")
            // } else {
            //     return ''
            // }
        }

        // await page.waitForSelector('.MenuOverlay-Item')
        // await page.click(
        //     '#root > header > nav > div.MenuOverlay-Container > div > div > ul > li')
        //
        //
        // await page.waitForSelector(
        //     '.ProductCard > .ProductCard-Link')
        // await page.click('.ProductCard > .ProductCard-Link')
        //
        // await page.waitForSelector('a[aria-hidden="false"].ProductAttributeValue')
        // const sizes = await page.$$('a[aria-hidden="false"].ProductAttributeValue')
        //
        // for (const size of sizes) {
        //     await size.click()
        //     break
        // }
        //
        // await page.waitForSelector('.ProductActions-AddToCartWrapper')
        // await page.click(
        //     '.ProductActions-Wrapper > .ProductActions > .ProductActions-AddToCartWrapper > .Button > ' +
        //     'span:nth-child(1)')
        // await page.waitForSelector('.Popup-Content')
        // await page.click(
        //     '.Popup-Content > .AddToCartPopup-Wrapper > .AddToCartPopup-Content > .AddToCartPopup-ButtonBlock > ' +
        //     '.AddToCartPopup-CheckoutButton')
        // await page.waitForSelector('.PreCheckout-Footer')
        //
        //
        // await page.waitForSelector('.PreCheckout-Footer > .PreCheckout-RegistrationOptions > .PreCheckout-RegistrationOptions-Buttons > ' +
        //     '.RegistrationOption > .Button'
        // )
        //
        // await page.click(
        //     '.PreCheckout-Footer > .PreCheckout-RegistrationOptions > .PreCheckout-RegistrationOptions-Buttons > ' +
        //     '.RegistrationOption > .Button'
        // )
        //
        // await page.waitForSelector('div > div > .CheckoutDeliveryOptions-Select > .CheckoutDeliveryOptions-TypeListOption > span')
        // await page.click('div > div > .CheckoutDeliveryOptions-Select > .CheckoutDeliveryOptions-TypeListOption > span')
        //
        // await page.click(
        //     `div > div > .CheckoutDeliveryOptions-TypeList > .CheckoutDeliveryOptions-TypeListOption:nth-child(1) >  button`)
        //
        // await page.click(
        //     `.CheckoutDeliveryOptions-OptionsWrapper > .CheckoutDeliveryOption:nth-child(1) > .CheckoutDeliveryOption-Button > .CheckoutDeliveryOption-Row > span`)
        //
        // await page.waitForSelector('#guest_email')
        // await page.type('#guest_email', 'test@automation.com')
        //
        // await page.type('#firstname', 'Test')
        //
        // await page.type('#lastname', 'Automation')
        //
        // await page.type('#telephone', '+37125555555')
        //
        // await page.type('#street', 'My Street')
        //
        // await page.type('#city', 'City')
        //
        // await page.type('#postcode', 'LV-3001')
        //
        // await page.click('#SHIPPING_STEP > div.Checkout-StickyButtonWrapper > button')
        //
        // await page.waitForSelector('#root > main > section > div > div.Checkout-Step > h1')
        //
        // await page.waitForSelector(`#BILLING_STEP > .CheckoutPayments > .CheckoutPayments-Methods > .CheckoutPayment:nth-child(1) > .CheckoutPayment-Button`)
        // await page.click(`#BILLING_STEP > .CheckoutPayments > .CheckoutPayments-Methods > .CheckoutPayment:nth-child(1) > .CheckoutPayment-Button`)
        //
        // await page.waitForSelector('#BILLING_STEP > div.CheckoutBilling-TermsAndConditions > div > label')
        // await page.click('#BILLING_STEP > div.CheckoutBilling-TermsAndConditions > div > label')
        //
        // await page.waitForTimeout(1000)

        const currentDate = new Date()
        const timestamp = currentDate.getTime()
        await page.screenshot({
            path: `${timestamp}.png`
        })
    });

    const sites = [
        'https://sportland.com',
        'https://sportland.ee',
        'https://sportland.lt',
        'https://sportland.lv'
    ]

    for (let i = 0; i <= 0; i++) {
        cluster.queue(sites[i]);
    }
    await cluster.idle();
    await cluster.close();
})();