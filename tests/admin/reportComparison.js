/**
 * @name Download Orders Returns Report and compare to list un Admin panel
 * @desc CSV is being downloaded, read and compared to actual list in Admin panel - report info is genereated
 */
const puppeteer = require('puppeteer')

const csv = require('csv-parser')
const fs = require('fs')
let countOfOrdersInCSV = -1
let countOfOrdersInAdmin = -1
let nameOfReport = ''

const selectorsForReportsToBeChecked =
[
    ['li[data-ui-id="menu-magento-reports-report-salesroot-orders-returns"]', 'span[data-ui-id="adminhtml-sales-ordersreturns-grid-total-count"]'],
    ['li[data-ui-id="menu-magento-reports-report-salesroot-orders-returns-old"]', 'span[data-ui-id="adminhtml-sales-ordersreturnsold-grid-total-count"]'],
    ['li[data-ui-id="menu-magento-reports-report-salesroot-sales-items"]', 'span[data-ui-id="adminhtml-sales-salesitems-grid-total-count"]']

]

let browser
let page
const website =
    'https://demo-sportland.readymage.com/sportskim2'

const dateFrom = '05/1/2021'
const dateTo = '05/4/2021'
const dir = './my-downloads'

adminLogin = 'dainis.anisimovs'
adminPass = 'Fj5Thc4JC9!vh6HJyUZ'

before(async () => {
    browser = await puppeteer.launch({
        headless: true,
        args: [
            '--start-fullscreen'
        ]
    })
    page = await browser.newPage()
    await page.setViewport({
        width: 1920,
        height: 1080
    })
    await page.setDefaultTimeout(600000)

    await page._client.send('Page.setDownloadBehavior', {behavior: 'allow', downloadPath: dir})
})

describe('Do Orders Returns Report comparison', function() {
    it('Having an access to storefront', async () => {
        await page.goto(website)//, {waitUntil: 'load', timeout: 0})

        await page.waitForSelector('.login-content')
    })

    it('Filling Admin login form and logging in', async function() {
        await page.type('#username', adminLogin)
        await page.type('#login', adminPass)
        await page.click('.action-login.action-primary')

        await page.waitForSelector('#menu-magento-reports-report')
    })

    for(const selectorForReportsToBeChecked of selectorsForReportsToBeChecked) {
        it('Click the Reports button in the Magento Admin Panel', async function() {
            await page.waitForTimeout(5000)
            for(let i = 0; i < 5; i++) {
                await page.click('#menu-magento-reports-report')
            }


            await page.waitForSelector(selectorForReportsToBeChecked[0] + ' span')
            const element = await page.$(selectorForReportsToBeChecked[0] + ' span')
            nameOfReport = await page.evaluate(element => element.textContent, element)

            await page.waitForSelector(selectorForReportsToBeChecked[0])
        })

        it('From the Reports drop-down menu, click to select specific type of REPORT', async function() {
            await page.waitForTimeout(2000)
            await page.click(selectorForReportsToBeChecked[0])

            await page.waitForSelector('#filter_form > #sales_report_base_fieldset #sales_report_from')
        })

        it('Choose report FROM date', async function() {
            await page.type('#filter_form > #sales_report_base_fieldset #sales_report_from', dateFrom)

            await page.waitForSelector('#filter_form > #sales_report_base_fieldset #sales_report_to')
        })

        it('Choose report TO date', async function() {
            await page.type('#filter_form > #sales_report_base_fieldset #sales_report_to', dateTo)

            await page.waitForSelector('#filter_form_submit')
        })

        it('Click on SHOW REPORT button', async function() {
            await page.waitForTimeout(1000)
            await page.click('#filter_form_submit')

            await page.waitForSelector('.action-default.scalable.task')
        })

        it('Click on EXPORT button', async function() {
            await page.waitForTimeout(1000)
            await page.click('.action-default.scalable.task')
        })

        it('Read downloaded .csv', async function() {
            const incrementIDs = []
            const results = []
            await page.waitForTimeout(1000)
            const fileNames = fs.readdirSync('./my-downloads')
            await page.waitForTimeout(1500)
            await fs.createReadStream(`./my-downloads/${fileNames[0]}`)
                .pipe(csv())
                .on('data', (data) => results.push(data))
                .on('end', () => {


                    for(const result of results){
                        incrementIDs.push(result["Increment ID"])
                    }

                })

            let checkCountOfOrdersInCSV = -1
            countOfOrdersInCSV = incrementIDs.length
            while(checkCountOfOrdersInCSV !== countOfOrdersInCSV) {
                checkCountOfOrdersInCSV = incrementIDs.length
                await page.waitForTimeout(1000)
                countOfOrdersInCSV = incrementIDs.length
            }
        })

        it('Read and output number of records found in Admin panel', async function() {
            await page.waitForSelector(selectorForReportsToBeChecked[1])
            const element = await page.$(selectorForReportsToBeChecked[1])
            countOfOrdersInAdmin = await page.evaluate(element => element.textContent, element)
        })

        it('Compare amount of order in downloaded CSV and admin panel', async function() {
            console.log('===========================================================================================')
            console.log(`${nameOfReport} - Amount of orders in CSV: ${countOfOrdersInCSV}`)
            console.log(`${nameOfReport} - Amount of orders in Admin: ${countOfOrdersInAdmin.trim()}`)
            console.log('===========================================================================================')
            console.log(`${nameOfReport} - Does order count in downloaded CSV matches count in Admin panel: ${countOfOrdersInCSV  == countOfOrdersInAdmin.trim() ? 'TRUE' : 'FALSE'}`)
            console.log('===========================================================================================')
        })

        it('Wait for 10 secs and delete my-downloads folder', async function() {
            fs.rmdir(dir, { recursive: true }, (err) => {
                if (err) {
                    throw err
                }
                console.log(`${dir} is deleted!`)
            })
        })
    }
})

after(async () => {
    await browser.close()
})