/**
 * @name Click Through all Menu items
 * @desc Lists of 404 and Empty Product Lists URLs are returned
 */
const playwright = require('playwright')

let browser
let page
const website =
    ['https://sportland.ee', 'kategooria']

let arrayOfPLPHeaders = []
let fourOhFourLinks = []
let emptyPLPLinks = []

/* List of Demo and Live websites
    ['https://sportland.com', 'category']
    ['https://sportland.ee', 'kategooria']
    ['https://sportland.lt', 'kategorija']
    ['https://sportland.lv', 'kategorija']

    ['https://demo-sportland.readymage.com', 'category']
    ['https://demo-sportland-ee.readymage.com', 'kategooria']
    ['https://demo-sportland-lt.readymage.com', 'kategorija']
    ['https://demo-sportland-lv.readymage.com', 'kategorija']

    ['https://sportland-en.indvp.com/', 'category']
    ['https://sportland-ee.indvp.com/', 'kategooria']
    ['https://sportland-lt.indvp.com/', 'kategorija']
    ['https://sportland.indvp.com/', 'kategorija']
*/

before(async () => {
    browser = await playwright.chromium.launch({
        headless: true,
        slowMo: 200
    })
    page = await browser.newPage()
    await page.setDefaultTimeout(600000)

    await page.setViewportSize({
        width: 1720,
        height: 1880
    })
})

describe('Click Through all Menu items', function() {
    this.timeout(5000000)

    it('Having an access to storefront', async () => {
        await page.goto(website[0], {waitUntil: 'load', timeout: 0})

        await page.waitForSelector('.Header-Button.Header-Button_type_account')
    })

    it('Having an access to Menu', async () => {
        await page.waitForSelector('.MenuOverlay-Menu')
    })

    it('Clicking through all Menu items', async () => {
        let counter = 0
        const ones = await page.$$('.MenuOverlay-ItemList.MenuOverlay-ItemList_type_main > li')
        await page.waitForTimeout(1000)
        console.log('--------------------------------------------------------------------------------------------------------------------')
        console.log('Website : ' + website[0])

        for (const one of ones) {
            counter++
            // if(counter == 1) continue
            // if(counter == 2) continue
            // if(counter == 3) continue
            // if(counter == 4) continue
            await one.hover()
            await one.click()
            await page.waitForSelector('div > h1')
            await page.waitForTimeout(1250)
            let element = await page.$('div > h1')
            let menuName = (await page.$('div > h1') !== null ? await page.evaluate(el => el.textContent, element) : 'BREADCRUMB HEADER CAN\'T BE READ')
            if(!menuName.includes('404')) arrayOfPLPHeaders.push(menuName)
            console.log('----------------------------------------------------------')
            console.log('├── ' + menuName + await doesURLHaveCategoryInIt(page.url()))
            menuName = 'could not read the PLP breadcrumb'
            if (counter > 4) continue

            const twos = await one.$$('div > div > div > div[role="button"]')
            await page.hover('#root > header > nav > div.MenuOverlay-Container > div > div > ul > li:nth-child(2) > div > a')
            await page.hover('#root > header > nav > div.MenuOverlay-Container > div > div > ul > li:nth-child(1) > div > a')
            await one.hover()

            for (const two of twos) {
                await page.waitForTimeout(3000)
                const threes = await two.$$('div > div > a')

                if (counter != 4) {
                    const clickable = await two.$$('a')
                    await page.hover('a')
                    await page.hover(`#root > header > nav > div.MenuOverlay-Container > div > div > ul > li:nth-child(${counter + 1}) > div > a`)
                    await page.hover(`#root > header > nav > div.MenuOverlay-Container > div > div > ul > li:nth-child(${counter}) > div > a`)
                    await one.hover()
                    await clickable[0].click()
                    await page.waitForSelector('div > h1')
                    await page.waitForTimeout(1000)
                    let subElement = await page.$('div > h1')
                    let subMenuName = (await page.$('div > h1') !== null ? await page.evaluate(el => el.textContent, subElement) : 'BREADCRUMB HEADER CAN\'T BE READ')
                    if(!subMenuName.includes('404')) arrayOfPLPHeaders.push(subMenuName)
                    console.log('----------------------------------------------------------')
                    console.log('├── ├──  ' + subMenuName + await doesURLHaveCategoryInIt(page.url()))
                    subMenuName = 'could not read the PLP breadcrumb'
                    console.log('----------------------------------------------------------')
                    await page.hover('a')
                    await one.hover()
                }

                for (const three of threes) {
                    await page.hover('a')
                    await one.hover()
                    await three.click()
                    await page.waitForSelector('div > h1')
                    await page.waitForTimeout(1000)
                    let subSubElement = await page.$('div > h1')
                    let subSubMenuName = (await page.$('div > h1') !== null ? await page.evaluate(el => el.textContent, subSubElement) : 'BREADCRUMB HEADER CAN\'T BE READ!')

                    let aaa = ((await page.$$('.CategoryProductList-ProductsMissing')).length)
                    if(aaa > 0){
                        console.log((await page.$$('.CategoryProductList-ProductsMissing')).length + '  ---  ' + page.url())
                        await emptyPLPLinkGatherer(page.url())
                    }


                    if (subSubMenuName.includes('404')) await fourOhFourLinkGatherer(subSubMenuName, page.url())

                    console.log('├── ├── ├── ' + subSubMenuName + await doesURLHaveCategoryInIt(page.url()))
                    if(!subSubMenuName.includes('404')) arrayOfPLPHeaders.push(subSubMenuName)

                    if(!subSubMenuName.includes('|') && subSubMenuName !== 'BREADCRUMB HEADER CAN\'T BE READ!') {
                        await page.waitForSelector('div > h1')
                        await page.waitForTimeout(1000)
                        const urls = await page.$$eval("figure > a", el => el.map(x => x.getAttribute("href")))
                        for(const url of urls){
                            const page2 = await browser.newPage()
                            await page2.setDefaultTimeout(600000)
                            await page2.setViewportSize({
                                width: 1920,
                                height: 2080
                            })
                            await page2.goto((url.search(website[0]) !== -1 ? '' : website[0]) + url, {waitUntil: 'load', timeout: 0})
                            await page2.waitForSelector('div > h1')
                            await page2.waitForTimeout(1000)
                            let subElement = await page2.$('div > h1')
                            let subMenuName = (await page2.$('div > h1') !== null ? await page2.evaluate(el => el.textContent, subElement) : 'BREADCRUMB HEADER CAN\'T BE READ')
                            for(let i = 0; i < 10; i++) {
                                if(subMenuName.length < 1) {
                                    await page.waitForTimeout(500)
                                    subMenuName = (await page2.$('div > h1') !== null ? await page2.evaluate(el => el.textContent, subElement) : 'BREADCRUMB HEADER CAN\'T BE READ')
                                }
                            }

                            let bbb = ((await page2.$$('.CategoryProductList-ProductsMissing')).length)
                            if(bbb > 0){
                                console.log((await page2.$$('.CategoryProductList-ProductsMissing')).length + '  ---  ' + page2.url())
                                await emptyPLPLinkGatherer(page2.url())
                            }

                            if(!subMenuName.includes('404')) arrayOfPLPHeaders.push(subMenuName)
                            if (subMenuName.includes('404')) {
                                await fourOhFourLinkGatherer(subMenuName, page2.url())
                                await page.waitForTimeout(3000)
                            }
                            console.log('├── ├── ├── ├── ' + subMenuName + await doesURLHaveCategoryInIt(page.url()))
                            subMenuName = 'could not read the PLP breadcrumb'
                            await page2.close()
                        }
                        console.log('===============================================================================================')
                    }
                }
            }
        }

        const doesArrayHaveDuplicates = arrayOfPLPHeaders.some(
            (val, i) => arrayOfPLPHeaders.indexOf(val) !== i
        )
        console.log('----------------------------------------------------------')
        console.log('Total amount of categories in Menu: ' + arrayOfPLPHeaders.length)
        console.log('Did automation script run into two identical breadcrumbs on PLP: ' + doesArrayHaveDuplicates)
        console.log('----------------------------------------------------------')
        console.log('How many 404 in opened PLP pages: ' + fourOhFourLinks.length)

        if (fourOhFourLinks.length > 0) {
            for (let fourOhFourLink of fourOhFourLinks) {
                console.log(fourOhFourLink)
            }
        }

        console.log('----------------------------------------------------------')
        console.log('How many EMPTY product list pages test ran in to: ' + emptyPLPLinks.length)

        if (emptyPLPLinks.length > 0) {
            for (let emptyPLPLink of emptyPLPLinks) {
                console.log(emptyPLPLink)
            }
        }

        async function fourOhFourLinkGatherer(header, url) {
            let trimmedHeader = header.trim()
            for(const fourOhFourLink of fourOhFourLinks) {
                if(fourOhFourLink == trimmedHeader) {
                    trimmedHeader = ''
                    break
                }
            }
            if (trimmedHeader == '404') fourOhFourLinks.push(url)
        }

        async function emptyPLPLinkGatherer(url) {
            emptyPLPLinks.push(url)
        }

        async function doesURLHaveCategoryInIt(url) {
            if(url.includes(website[1])) {
                return (` / [URL with name "${website[1]}" in it : ` +  url + "]")
            } else {
                return ''
            }
        }
    }).timeout(5000000)
})

after(async () => {
    await browser.close()
})