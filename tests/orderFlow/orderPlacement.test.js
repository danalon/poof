/**
 * @name Order placement
 * @desc Order number appearing after successful login, product addition to cart, checkout and payment completion
 */
const playwright = require('playwright')

let browser
let page
const website =
    ['https://sportland.lv/', '1621243940615@automation.com', 'exit-before-placing-order']

/*
    ['https://demo-sportland.readymage.com/', '1621238478703@automation.com']
    ['https://demo-sportland-ee.readymage.com/', '1621238249571@automation.com']
    ['https://demo-sportland-lt.readymage.com/', '1621238374315@automation.com']
    ['https://demo-sportland-lv.readymage.com/', '1621238841850@automation.com']

    ['https://sportland.com/', '1621242456380@automation.com', 'exit-before-placing-order']
    ['https://sportland.ee/', '1621242857415@automation.com', 'exit-before-placing-order']
    ['https://sportland.lt/', '1621243845324@automation.com', 'exit-before-placing-order']
    ['https://sportland.lv/', '1621243940615@automation.com', 'exit-before-placing-order']
*/

const email = '1621237831029@automation.com'
const password = 'NKkn*8h98fs98hvc&&('

before(async () => {
    browser = await playwright.chromium.launch({
        headless: false
    })
    page = await browser.newPage()

    await page.setViewportSize({
        width: 1920,
        height: 1080
    })
})

describe('Order placement', function() {
    this.timeout(5000)

    it('Having an access to storefront', async () => {
        await page.goto(website[0])

        await page.waitForSelector('.Header-Button.Header-Button_type_account')
    }).timeout(10000)

    it('Clicking on Register/Sign In button', async () => {
        await page.click('.Header-Button.Header-Button_type_account')
    })

    it('Sign in to your account drop-down appearing', async () => {
        await page.waitForSelector('.Button.Button_isHollow')
    })

    it('Filling Login or Email field with valid info', async () => {
        await page.fill('#email', website[1])
    })

    it('Filling Password field with valid info', async () => {
        await page.fill('#password', password)
    })

    it('Clicking on SIGN IN button', async () => {
        await page.click('.Button')
    })

    it('Redirecting to My Account Dashboard page', async () => {
        await page.waitForSelector('.MyAccountTabListItem_tab_Address_book > .MyAccountTabListItem-Button')
    })

    it('Clicking on last item listed in Main Menu', async () => {
        await page.click(
            '#root > header > nav > div.MenuOverlay-Container > div > div > ul > li:nth-child(5)')
    })

    it('PLP page appears', async () => {
        await page.waitForSelector(
            '.ProductCard > .ProductCard-Link')
    })

    it('Clicking on first item listed in PLP', async () => {
        await page.click('.ProductCard > .ProductCard-Link')
    })

    it('PDP page appears', async () => {
        await page.waitForSelector('.ProductActions-AddToCartWrapper')
    })

    it('Clicking through all available size options', async () => {
        await page.waitForSelector('a[aria-hidden="false"].ProductAttributeValue')
        const sizes = await page.$$('a[aria-hidden="false"].ProductAttributeValue')
        for(const size of sizes){
            await size.click()
            break
        }
    })

    it('Clicking on ADD TO CART button', async () => {
        await page.click(
            '.ProductActions-Wrapper > .ProductActions > .ProductActions-AddToCartWrapper > .Button > ' +
            'span:nth-child(1)')
    })

    it('Pop-up with chosen product info and CHECKOUT button appears', async () => {
        await page.waitForSelector('.Popup-Content')
    })

    it('Clicking on CHECKOUT button', async () => {
        await page.click(
            '.Popup-Content > .AddToCartPopup-Wrapper > .AddToCartPopup-Content > .AddToCartPopup-ButtonBlock > ' +
            '.AddToCartPopup-CheckoutButton')
    })

    it('Checkout page appears with Carrier method: DPD pre-selected', async () => {
        await page.waitForSelector('.Checkout-ProgressBar')
    })

    it('Clicking on first option from Select terminal drop-down', async () => {
        await page.click('div > div > .CheckoutDeliveryOptions-Select > .CheckoutDeliveryOptions-TypeListOption > span')
        await page.click(
            'div > div > .CheckoutDeliveryOptions-TypeList > .CheckoutDeliveryOptions-TypeListOption:nth-child(1) >' +
            ' button')
        await page.click(
            '.CheckoutDeliveryOptions-OptionsWrapper > .CheckoutDeliveryOption:nth-child(1) > ' +
            '.CheckoutDeliveryOption-Button > .CheckoutDeliveryOption-Row > span')
    })

    it('Clicking on PROCEED TO BILLING button', async () => {
        await page.click('#SHIPPING_STEP > div.Checkout-StickyButtonWrapper > button')
    })

    it('Billing Step page appears with My billing and shipping are the same', async () => {
        await page.waitForSelector('#root > main > section > div > div.Checkout-Step > h1')
    })

    it('Checking I agree to terms and conditions checkbox', async () => {
        await page.click('#BILLING_STEP > div.CheckoutBilling-TermsAndConditions > div > label')
    })

    if(!website[2]){
        it('Clicking on COMPLETE ORDER button', async () => {
            await page.click('#BILLING_STEP > div.Checkout-StickyButtonWrapper > button')
        })

        it('Thank you for your purchase! page appears', async () => {
            await page.waitForSelector('#root > main > section > div > div.Checkout-Step > div > h3')
            const orderNumber = await page.$eval(
                '#root > main > section > div > div.Checkout-Step > div > h3',
                el => el.innerText.match(/(\d+)/)
            )
            console.log('Order number: ' + orderNumber[0])
        })
    }
})

after(async () => {
    await browser.close()
})