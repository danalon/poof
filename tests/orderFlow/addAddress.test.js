/**
 * @name Add Address to the User's address book
 * @desc New address appearing in Address book after logging in and adding new address
 */
const playwright = require('playwright')
const addContext = require('mochawesome/addContext')

let browser
let page
const website =
    ['https://sportland.lv/', '+37167389478', 'LV-3001', '1621243940615@automation.com']

/*
    ['https://demo-sportland.readymage.com/', '+37167389478', 'LV-3001', '1621238478703@automation.com']
    ['https://demo-sportland-ee.readymage.com/', '+3726738947', '12345', '1621238249571@automation.com']
    ['https://demo-sportland-lt.readymage.com/', '+37067389478', 'LT-30012', '1621238374315@automation.com']
    ['https://demo-sportland-lv.readymage.com/', '+37167389478', 'LV-3001', '1621238841850@automation.com']

    ['https://sportland.com/', '+37167389478', 'LV-3001', '1621242456380@automation.com']
    ['https://sportland.ee/', '+3726738947', '12345', '1621242857415@automation.com']
    ['https://sportland.lt/', '+37067389478', 'LT-30012', '1621243845324@automation.com']
    ['https://sportland.lv/', '+37167389478', 'LV-3001', '1621243940615@automation.com']
*/
const firstName = 'Test'
const lastName = 'Automation'
const password = 'NKkn*8h98fs98hvc&&('
const city = 'Ādaži'
const street = 'Lielā iela 1'

before(async () => {
    browser = await playwright.chromium.launch({
        headless: true
    })
    page = await browser.newPage()

    await page.setViewportSize({
        width: 1920,
        height: 1080
    })
})

beforeEach(function () {
    addContext(this, 'some context')
})

afterEach(function () {
    addContext(this, {
        title: 'afterEach context',
        value: { a: 1 },
    });
})

describe('Add Address to the User\'s address book', function() {
    this.timeout(500000)

    it('Having an access to storefront', async () => {
        await page.goto(website[0])

        await page.waitForSelector('.Header-Button.Header-Button_type_account')
    }).timeout(10000)

    it('Clicking on Register/Sign In button', async () => {
        await page.click('.Header-Button.Header-Button_type_account')
    })

    it('Sign in to your account drop-down appearing', async () => {
        await page.waitForSelector('.Button.Button_isHollow')
    })

    it('Filling Login or Email field with valid info', async () => {
        await page.fill('#email', website[3])
    })

    it('Filling Password field with valid info', async () => {
        await page.fill('#password', password)
    })

    it('Clicking on SIGN IN button', async () => {
        await page.click('.Button')
    })

    it('Redirecting to My Account Dashboard page', async () => {
        await page.waitForSelector('.MyAccountTabListItem_tab_Address_book > .MyAccountTabListItem-Button')
    })

    it('Clicking on Address book option in sidebar', async () => {
        await page.click('.MyAccountTabListItem_tab_Address_book > .MyAccountTabListItem-Button')
    })

    it('List off addresses in Address book appears', async () => {
        await page.waitForSelector('.MyAccountAddressBook-Button')
    })

    it('Clicking on ADD NEW ADDRESS button', async () => {
        await page.click(
            'section > .ContentWrapper > .MyAccount-TabContent > .MyAccountAddressBook > .MyAccountAddressBook-Button')
    })

    it('Add new address pop-up appears', async () => {
        await page.waitForSelector(
            '.Popup-Content > .Form > .FieldForm-Fields > .Field:nth-child(1) > label:nth-child(3)')
    })

    it('Checking This is default Billing Address checkbox', async () => {
        await page.click('.Popup-Content > .Form > .FieldForm-Fields > .Field:nth-child(1) > label:nth-child(3)')
    })

    it('Checking This is default Shipping Address checkbox', async () => {
        await page.click('.Popup-Content > .Form > .FieldForm-Fields > .Field:nth-child(2) > label:nth-child(3)')
    })

    it('Filling First name field with valid info', async () => {
        await page.fill('#firstname', firstName)
    })

    it('Filling Last name field with valid info', async () => {
        await page.fill('#lastname', lastName)
    })

    it('Filling Phone number field with valid info', async () => {
        await page.fill('#telephone', website[1])
    })

    it('Filling City field with valid info', async () => {
        await page.fill('#city', city)
    })

    // it('Clicking on Latvia option from Country drop-down', async () => {
    //     await page.waitForSelector('.Popup-Content > .Form > .FieldForm-Fields > .Field:nth-child(7) > .Field-SelectWrapper')
    //     await page.click('.Popup-Content > .Form > .FieldForm-Fields > .Field:nth-child(7) > .Field-SelectWrapper')
    //
    //     await page.waitForSelector('.FieldForm-Fields #oLV')
    //     await page.click('.FieldForm-Fields #oLV')
    // })
    //
    // it('Clicking on first option from State/Province drop-down', async () => {
    //     await page.waitForSelector('.Popup-Content > .Form > .FieldForm-Fields > .Field:nth-child(8) > .Field-SelectWrapper')
    //     await page.click('.Popup-Content > .Form > .FieldForm-Fields > .Field:nth-child(8) > .Field-SelectWrapper')
    //
    //     await page.waitForSelector('.FieldForm-Fields #o380')
    //     await page.click('.FieldForm-Fields #o380')
    // })

    it('Filling Zip/Postal code field with valid info', async () => {
        await page.fill('#postcode', website[2])
    })

    it('Filling Street address field with valid info', async () => {
        await page.fill('#street', street)
    })

    it('Clicking on SAVE ADDRESS button', async () => {
        await page.click('button.Button.MyAccount-Button')
    })

    it('List off addresses in Address book appears with newly added address', async () => {
        await page.waitForSelector('.KeyValueTable-Wrapper')
    })
})

after(async () => {
    await browser.close()
})