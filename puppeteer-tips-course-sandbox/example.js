// // screenshot
// const puppeteer = require('puppeteer');
//
// (async () => {
//     const browser = await puppeteer.launch()
//     const page = await browser.newPage()
//     await page.goto('https://www.example.com', {waitUntil: 'networkidle0'})
//     await page.screenshot({path: 'example.png', fullPage: true})
//     await browser.close()
// })()

// // open as in mobile device
// const puppeteer = require('puppeteer');
// const iPhone = puppeteer.devices['iPhone 6'];
//
// (async () => {
//     const browser = await puppeteer.launch({headless: false})
//     const page = await browser.newPage()
//     await page.emulate(iPhone)
//     await page.goto('https://www.sportland.com', {waitUntil: 'networkidle0'})
//
//     await page.waitForTimeout(3000)
//
//     await browser.close()
// })()

// // changing geolocation
// const puppeteer = require('puppeteer');
//
// (async () => {
//     const browser = await puppeteer.launch({headless: false})
//     const page = await browser.newPage()
//
//     const context = browser.defaultBrowserContext()
//     await context.overridePermissions('https://chercher.tech/practice/geo-location', ['geolocation'])
//
//     await page.setGeolocation({latitude: 90, longitude: 20})
//
//     await page.goto('https://chercher.tech/practice/geo-location')
//
//     await page.waitForTimeout(10000)
//
//     await browser.close()
// })()

// // checking accessibility
// const puppeteer = require('puppeteer');
//
// (async () => {
//     const browser = await puppeteer.launch({headless: false})
//     const page = await browser.newPage()
//     await page.goto('https://pptr.dev')
//     await page.waitForSelector('h1')
//
//     const snapshot = await page.accessibility.snapshot()
//     console.log(snapshot)
//
//     // await page.waitForTimeout(10000)
//
//     await browser.close()
// })()

// // checking accessibility
// const puppeteer = require('puppeteer');
// const fs = require('fs');
//
// (async () => {
//     const browser = await puppeteer.launch({headless: false})
//     const page = await browser.newPage()
//     await page.goto('https://pptr.dev')
//     await page.waitForSelector('h1')
//
//     const snapshot = await page.accessibility.snapshot({
//         interestingOnly: false,
//         root: await page.$('h1')
//     })
//     // fs.writeFile('output.json', JSON.stringify(snapshot, null, 4), () => {})
//     console.log(JSON.stringify(snapshot, null, 4))
//
//     // await page.waitForTimeout(10000)
//
//     await browser.close()
// })()

// // launching test in firefox
// const puppeteer = require('puppeteer-firefox');
//
// (async () => {
//     const browser = await puppeteer.launch({headless: false})
//     const page = await browser.newPage()
//
//     await page.goto('https://pptr.dev')
//     await page.waitForSelector('h1')
//     await browser.close()
// })()

// launching test in firefox
// checking accessibility
const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({headless: false})
    const page = await browser.newPage()

    await page.goto('https://demo-sportland.readymage.com')

    await page.setViewport({
        width: 1920,
        height: 1080 * 2
    })

    await page.waitForSelector('.MenuOverlay-Item')
    await page.click(
        '#root > header > nav > div.MenuOverlay-Container > div > div > ul > li')
    await page.waitForSelector(
        '.ProductCard > .ProductCard-Link')
    await page.click('.ProductCard > .ProductCard-Link')
    await page.waitForSelector('.ProductActions-AddToCartWrapper')
    await page.waitForSelector('a[aria-hidden="false"].ProductAttributeValue')
    const sizes = await page.$$('a[aria-hidden="false"].ProductAttributeValue')
    for(const size of sizes){
        await size.click()
        break
    }
    await page.click(
        '.ProductActions-Wrapper > .ProductActions > .ProductActions-AddToCartWrapper > .Button > ' +
        'span:nth-child(1)')
    await page.waitForSelector('.Popup-Content')
    await page.click(
        '.Popup-Content > .AddToCartPopup-Wrapper > .AddToCartPopup-Content > .AddToCartPopup-ButtonBlock > ' +
        '.AddToCartPopup-CheckoutButton')
    await page.waitForSelector('.PreCheckout-Footer')


    await page.waitForSelector(            '.PreCheckout-Footer > .PreCheckout-RegistrationOptions > .PreCheckout-RegistrationOptions-Buttons > ' +
        '.RegistrationOption > .Button'
    )

    await page.click(
        '.PreCheckout-Footer > .PreCheckout-RegistrationOptions > .PreCheckout-RegistrationOptions-Buttons > ' +
        '.RegistrationOption > .Button'
    )

    await page.waitForSelector('div > div > .CheckoutDeliveryOptions-Select > .CheckoutDeliveryOptions-TypeListOption > span')
    await page.click('div > div > .CheckoutDeliveryOptions-Select > .CheckoutDeliveryOptions-TypeListOption > span')

    await page.click(
        `div > div > .CheckoutDeliveryOptions-TypeList > .CheckoutDeliveryOptions-TypeListOption:nth-child(1) >  button`)

    await page.click(
        `.CheckoutDeliveryOptions-OptionsWrapper > .CheckoutDeliveryOption:nth-child(1) > .CheckoutDeliveryOption-Button > .CheckoutDeliveryOption-Row > span`)

    await page.waitForSelector('#guest_email')
    await page.type('#guest_email', 'test@automation.com')

    await page.type('#firstname', 'Test')

    await page.type('#lastname', 'Automation')

    await page.type('#telephone', '+37125555555')

    await page.type('#street', 'My Street')

    await page.type('#city', 'City')

    await page.type('#postcode', 'LV-3001')

    await page.click('#SHIPPING_STEP > div.Checkout-StickyButtonWrapper > button')

    await page.waitForSelector('#root > main > section > div > div.Checkout-Step > h1')

    await page.waitForSelector(`#BILLING_STEP > .CheckoutPayments > .CheckoutPayments-Methods > .CheckoutPayment:nth-child(1) > .CheckoutPayment-Button`)
    await page.click(`#BILLING_STEP > .CheckoutPayments > .CheckoutPayments-Methods > .CheckoutPayment:nth-child(1) > .CheckoutPayment-Button`)

    await page.waitForSelector('#BILLING_STEP > div.CheckoutBilling-TermsAndConditions > div > label')
    await page.click('#BILLING_STEP > div.CheckoutBilling-TermsAndConditions > div > label')


    await page.waitForTimeout(1000)

    const currentDate = new Date()
    const timestamp = currentDate.getTime()
    await page.screenshot({ path: `screenshot${Date.now()}.png` })



    await browser.close()
})()